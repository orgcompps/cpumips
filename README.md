# Descrição em português #

Simulador da CPU/MIPS multiciclo utilizando a biblioteca pthreads

Professor: Paulo Sérgio Lopes de Souza

Disciplina: Sistemas Operacionais

Alunos:

* Caio César de Almeida Guimarães              Nº USP: 8551497
* Helder de Mello Mendes                       Nº USP: 8504351
* Henrique Cintra Miranda de Souza Aranha      Nº USP: 8551434
* Lucas Eduardo de Mello                       Nº USP: 8504218

# English description #

Multi-cyle MIPS CPU simulator using the pthreads library

Professor: Paulo Sérgio Lopes de Souza

Course: Operating Systems

Alunos:

* Caio César de Almeida Guimarães              Nº USP: 8551497
* Helder de Mello Mendes                       Nº USP: 8504351
* Henrique Cintra Miranda de Souza Aranha      Nº USP: 8551434
* Lucas Eduardo de Mello                       Nº USP: 8504218