#ifndef _REGISTER_H_
#define _REGISTER_H_

#include "global.h"

int reg[32]; // Banco de registradores.

void *register_bank(); // Lógica do banco de registradores.

typedef struct sRegister_data {
    int nextValue;                            // Proximo valor que sera armazenado no registrador (na transição de clock).
    int currentValue;                        // Valor atual armazenado no registrador.
    int dependencies;

    sem_t sinc;                   // Impede que componentes dependentes desse registrador acessem seu valor antes do valor ser alterado
} st_register_data;

/*
 * Registradores (variáveis globais)
*/
st_register_data *MDR;       // Registrador de dados da memoria
st_register_data *A, *B;     // Operandos
st_register_data *ALUOut;    // Saida da ALU

/* Inicialização de um registrador.
* - register_data é um ponteiro para a struct que representará o registrador.
* - value corresponde ao valor inicial do registrador no momento de criação.
* - Retorno: Positivo se sucesso, Zero ou Negativo se ocorre erro.
*/
st_register_data *register_init( int value, int dependent_components );

// Desalocagem de recursos do registrador.
void register_destroy( st_register_data *register_data );

// Atualiza o valor de um registrador.
void *register_update( void *m_data );

// Funcionmento do banco de registradores.
void *register_bank( void *m_data );

// Imprimir registradores no banco.
void regbank_print();

#endif // _REGISTER_H_
