#ifndef _IR_H_
#define _IR_H_

#include "global.h"
#include <pthread.h>
#include <math.h>
#include <stdio.h>

typedef struct instruction_register {
	int nextValue;								// Proximo valor que sera armazenado
	int currentValue;							// Valor atual instrução armazenada no IR.

	sem_t sinc;

    int previousIRWrite;

	int output_31_26;		//Instruction [31:26]
	int output_25_21;		//Instruction [25:21]
	int output_20_16;		//Instruction [20:16]
	int output_15_0;		//Instruction [15:0]

	int output_25_0;		//Instruction [25:0]
	int output_15_11;		//Instruction [15:11]
	int output_5_0;			//Instruction [5:0]
} st_ir_data;

st_ir_data *IR;

/* Retorna o escopo de bits desejado [ibit:ebit] presente na instrução recebida. */
int instruction_to_bit(unsigned int inst, unsigned int ibit, unsigned int ebit);

/* Inicialização do Instruction Register. */
int ir_init();

void ir_destroy();

/* Funcionamento do Instruction Register. */
void *ir( void * );

#endif
