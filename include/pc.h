#ifndef _PC_H_
#define _PC_H_

#include "register.h"

st_register_data *PC;
int previousSignal;

// Inicialização do PC.
int pc_init();

// Destrutor do PC.
void pc_destroy();

// Funcionamento do PC.
void *pc( void * );

// Funcionamento das portas lógicas AND e OR que controlam a escrita em PC.
void *pc_and_or_gates( void * );

#endif // _PC_H_
