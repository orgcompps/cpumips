#ifndef _ALU_H_
#define _ALU_H_

#include "memory.h"
#include "register.h"
#include "mux.h"

//Referencias enviados pela ALU control para a ALU.
#define alu_add 0x02	// 00000010
#define alu_sub 0x06
#define alu_and 0x00
#define alu_or 0x01
#define alu_slt 0x07

//Dados lidos do campo 'funct' da instrução.
#define funct_add 0x20	// 00100000
#define funct_sub 0x22
#define funct_and 0x24
#define funct_or 0x25
#define funct_slt 0x2A

typedef struct alu_control_data{
	int *input;					//Entrada para o campo 'funct' da instrução [5:0].
	int *alu_op_input;			//Entrada para sinal de 2 bits recebido da Unidade de Controle.

	int output;					//Saída de controle para a ALU.
	sem_t sinc;                //Semáforo para aceso a saída da Alu Control

} st_alu_control_data;

typedef struct alu_data {
	int *control_op;

	int result;					//Saída da ALU.
	int zero;
	sem_t sinc;       //Semáforo para aceso as saídas da Alu
} st_alu_data;

st_alu_control_data *alu_control;
st_alu_data *alu;

/* Inicialização da ALU. */
int alu_init();

/* Desalocação de recursos de memória. */
void alu_destroy();

/* Funcionamento da ALU. */
void *alu_update(void *m_data);

/* Inicialização da ALU Control. */
int alu_control_init();

/* Desalocação de recursos de memória. */
void alu_control_destroy();

/* Funcionamento da ALU Control. */
void *alu_control_update(void *m_data);

#endif
