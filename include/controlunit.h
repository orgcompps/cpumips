#ifndef _CONTROLUNIT_H_
#define _CONTROLUNIT_H_

#include "global.h"

enum states {
    INSTRUCTION_FETCH,
    INSTRUCTION_DECODE,
    JUMP_COMPLETION,
    BRANCH_COMPLETION,
    EXECUTION,
    RTYPE_COMPLETION,
    MEMORY_ADDRESS_COMPUTATION,
    MEMORY_ACCESS_SW,
    MEMORY_ACCESS_LW,
    MEMORY_READ_COMPLETION
};

enum control_signals {
    PCWRITECOND,
    PCWRITE,
    IORD,
    MEMREAD,
    MEMWRITE,
    MEMTOREG,
    IRWRITE,
    PCSOURCE,
    ALUOP,
    ALUSRCB,
    ALUSRCA,
    REGWRITE,
    REGDST,
    SIGNAL_NUM
};

enum instructions {
    INSTRUCTION_J = 0x02,
    INSTRUCTION_BEQ = 0x04,
    INSTRUCTION_R = 0x00,
    INSTRUCTION_LW = 0x23,
    INSTRUCTION_SW = 0x2b
};

typedef struct sControlUnit {
    int controlSignal[SIGNAL_NUM];

    int currentState;

    sem_t sinc;

    int shutdown;
} st_control_unit;

st_control_unit *control_unit;

int controlunit_init( int state );

void *controlunit_update();

void controlunit_destroy();

#endif //_CONTROLUNIT_H_
