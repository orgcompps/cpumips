#ifndef _MUX_H_
#define _MUX_H_

#include <stdarg.h>
#include <semaphore.h>

#include "global.h"

enum muxNames { // Os multiplexadores são nomeados segundo seu sinal de seleção.
    MUX_IORD,
    MUX_MEMTOREG,
    MUX_REGDEST,
    MUX_ALUSRCA,
    MUX_ALUSRCB,
    MUX_PCSOURCE,
    MUX_NUM
};

typedef struct mux_data {
    int input_count;                                // Quantidade de entradas do multiplexador.

    int output;                                       // Saída do multiplexador.
    sem_t control;                               // Controla o acesso à variável de saída do multiplexador.

    int *selector;                                  // Código seletor.
    sem_t *selector_control;      // Controla o acesso à variável do código seletor.

    int **input;                                   // Ponteiros para as entradas do multiplexador.
    sem_t **input_control;        // Ponteiros para as variáveis de controle da entrada.
} st_mux_data;

st_mux_data *muxers[MUX_NUM]; // Vetor de multiplexadores

/* Inicialização de um multiplexador.
* - mux_data é um ponteiro para a struct que representará os dados contidos no multiplexador.
* - selector corresponde a um ponteiro à variável responsável pelo código de seleção.
* - selector_control corresponde ao semáforo utilizado para controlar o acesso a essa variável, assume-se que
* ele é criado externamente ao multiplexador.
* - Após input_count fornecer ponteiros para as entradas ex: &PCWrite, &PCWriteCond
* - Após fornecer ponteiros para as entradas, fornecer ponteiros para as variáveis de controle das entradas.
* - Retorno: Positivo se sucesso, Zero ou Negativo se ocorre erro.
*/
st_mux_data *mux_init( int *selector, sem_t *selector_control, int input_count, ... );

// Desalocagem de recursos do multiplexador.
void mux_destroy( st_mux_data *mux_data );

// Funcionamento de um multiplexador.
void *mux( void *m_data );

void *mux_pcsource( void *m_data );

#endif // _MUX_H_
