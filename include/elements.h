#ifndef _ELEMENTS_H_
#define _ELEMENTS_H_

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

// Estrutura genérica representando tanto o shift left como o sign extend.
typedef struct element_data {
	int *input;
	sem_t *input_sinc;

	int output;
	sem_t sinc;
} st_element_data;

int concatenator_output;
sem_t concatenator_sinc;

/*Inicialização do Deslocador à esquerda. */
st_element_data *element_init(int *input, sem_t *input_sinc);

void element_destroy( st_element_data *element );

/* Funcionamento do Deslocador à esquerda. */
void *shift_left(void *shift_l);

/* Funcionamento do Extensor de Sinal. */
void *signal_extend(void *se_data);

/* Inicialização do concatenador
* Retorno: Zero ou negativo se ocorrer erro. Positivo se sucesso.
*/
int concatenator_init();

/* Funcionamento do concatenador */
void *concatenator();

void concatenator_destroy();

#endif
