#ifndef _MEMORY_H_
#define _MEMORY_H_

#include "register.h"

#define RAM_SIZE 200 // Alterar aqui o tamanho, em palavras, da memória RAM

/*
 * Memória RAM
*/
int RAM[RAM_SIZE];
int originalRAM[RAM_SIZE];		//Cópia da RAM inicial.

void *ram_update(void *m_data);

#endif // _MEMORY_H_
