#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>

// Declaração de globais para sincronização.
pthread_barrier_t clock_signal;

inline void sem_multipost( sem_t *sem, unsigned int value );

#endif // _GLOBAL_H_
