CC = gcc
CFLAGS = -g #-Og -Wall

INCLUDE = -Iinclude/

LIBS = -lpthread -lm

CL = $(wildcard src/*.c)    #cpp list
HL = $(wildcard include/*.h)    #header list
OL = $(patsubst src/%.c, obj/%.o, $(CL) )  #object list

all: $(OL); $(CC) $(CFLAGS) $(OL) $(LIBS) $(INCLUDE) -o cpumips

obj/%.o: src/%.c; $(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@

run: ; ./cpumips

clean: ; rm -f cpumips obj/*.o
