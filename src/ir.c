#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "global.h"
#include "memory.h"
#include "mux.h"
#include "controlunit.h"
#include "register.h"
#include "ir.h"

st_ir_data *IR = NULL;

int instruction_to_bit(unsigned int inst, unsigned int ibit, unsigned int ebit){
    unsigned int upp_mask = pow(2, ibit+1)-1;		// Máscara superior. 1º bit incluso. Ex: 01111111
    unsigned int low_mask = pow(2, ebit)-1;			// Máscara inferioir. 1º bit nao incluso. Ex: 00001111

    //XOR lógico entre as duas máscaras
    unsigned int mask = upp_mask ^ low_mask;		//Ex: 01110000

    inst = inst & mask;		//AND lógico da máscara com a instrução.
    inst = inst >> ebit;	//Deslocamento da instrução desprezando os 'ebit' bits menos significativos.
    return (int)inst;
}

int ir_init() {
    if( IR != NULL )
    	return -1;
    if ( (IR = (st_ir_data*)malloc( sizeof(st_ir_data) ) ) == NULL )
        return -2;
    if ( sem_init( &IR->sinc, 0, 0 ) != 0 ){
    	free(IR);
    	IR = NULL;
        return -3;
	}

    IR->previousIRWrite = 0;

	return 1;
}

void ir_destroy(){
	sem_destroy(&IR->sinc);
	free(IR);
	IR = NULL;
}

void *ir( void *m_data ){

	while( control_unit->shutdown == 0 ){

		pthread_barrier_wait( &clock_signal );

		if ( IR->previousIRWrite == 1 ){
			IR->currentValue = IR->nextValue;
		}

		//Atualização dos valores de saída do IR.
		IR->output_31_26 = instruction_to_bit( (unsigned int)IR->currentValue, 31, 26);
		IR->output_25_21 = instruction_to_bit( (unsigned int)IR->currentValue, 25, 21);
		IR->output_20_16 = instruction_to_bit( (unsigned int)IR->currentValue, 20, 16);
		IR->output_15_0  = instruction_to_bit( (unsigned int)IR->currentValue, 15, 0);

		IR->output_25_0  = instruction_to_bit( (unsigned int)IR->currentValue, 25, 0);
		IR->output_15_11 = instruction_to_bit( (unsigned int)IR->currentValue, 15, 11);
		IR->output_5_0   = instruction_to_bit( (unsigned int)IR->currentValue, 5, 0);

		sem_multipost( &IR->sinc, 8 );

        sem_wait( &control_unit->sinc );
		IR->previousIRWrite = control_unit->controlSignal[IRWRITE];

        pthread_barrier_wait( &clock_signal );
	}

	pthread_exit(0);
}
