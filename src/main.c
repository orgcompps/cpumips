/** Trabalho 1 - Sistemas Operacionais
* Simulador da CPU/MIPS multiciclo.
*
*  Professor: Paulo Sérgio Lopes de Souza
* Alunos:
* Caio César de Almeida Guimarães                  Nº USP: 8551497
* Helder de Mello Mendes                                Nº USP: 8504351
* Henrique Cintra Miranda de Souza Aranha      Nº USP: 8551434
* Lucas Eduardo de Mello                                Nº USP: 8504218
*/

// Biblioteca padrão
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

// Includes do projeto
#include "global.h"
#include "memory.h"
#include "mux.h"
#include "register.h"
#include "pc.h"
#include "controlunit.h"
#include "elements.h"
#include "ir.h"
#include "alu.h"

/* Função que escreve o programa na RAM
 *
 * As instruções começam em PC[0] e são executadas sequencialmente a menos que existam branches ou jumps
 *
 * Todos os endereços dentro de instruções devem ser multiplos de 4, pois para manter a semantica do mips utilizamos endereçamento a bytes no caminho de dados, mas endereçamento a palavras na memória RAM
 *
 * Para indicar o final do segmento de texto deve-se utilizar uma instrução invalida. Recomendamos, por exemplo, a instrução 0xFFFFFFFF. Lembrando que a instrução 0 é considerada válida, pois possui opcode de um instrução tipo-R.
 *
 * Todo o conteudo da RAM posterior a essa instrução inválida pode ser usado como segmento de dados ou pilha.
*/
void write_program()
{
    reg[2] = 84; // v0
    reg[11] = 8; // t3
    reg[12] = 2; // t4
    reg[13] = 100; // t5

    RAM[0] = 0b10001100010010000000000000000000;  // lw $t0, 0($v0)
    RAM[1] = 0b00000001000011000001100000101010;  // slt $v1, $t0, $t4
    RAM[2] = 0b00010000011011010000000000000100;  // beq $v1, $t5, fim(4 palavras abaixo de PC+4)
    RAM[3] = 0b00000001000010110100000000100010;  // sub $t0, $t0, $t3
    RAM[4] = 0b10101100010010000000000000000000;  // sw $t0, 0($v0)
    RAM[5] = 0b00000001011000100001000000100000;  // add $v0, $t3, $v0
    RAM[6] = 0xFFFFFFFF;           // fim
    
    // Dados
    RAM[21] = 20;
    RAM[22] = 22;
    RAM[23] = 24;
}

// Nomes das threads
enum threadNames {
    THREAD_UC,
    THREAD_PC,
    THREAD_PC_AND_OR_GATES,
    THREAD_MUX_IORD,
    THREAD_MEMORY,
    THREAD_IR,
    THREAD_MDR,
    THREAD_MUX_MEMTOREG,
    THREAD_MUX_REGDEST,
    THREAD_REGBANK,
    THREAD_SIGNEXTEND,
    THREAD_SL_EXTEND,
    THREAD_A,
    THREAD_B,
    THREAD_MUX_ALUSRCA,
    THREAD_MUX_ALUSRCB,
    THREAD_SL_IR,
    THREAD_CONCATENATOR,
    THREAD_ALUCONTROL,
    THREAD_ALU,
    THREAD_ALUOUT,
    THREAD_MUX_PCSOURCE,
    THREAD_NUM
};

int main()
{
    int i;
    int four = 4; // Para uso no multiplexador ALUSrcB

    // Declaração das threads.
    pthread_t threads[THREAD_NUM];

    // Inicialização da barreira (clock_signal).
    pthread_barrier_init( &clock_signal, 0, THREAD_NUM + 1 );

    // Inicialização das unidades.
    controlunit_init(INSTRUCTION_FETCH);
    pc_init();
    concatenator_init();
    ir_init();
    alu_control_init();
    alu_init();
    assert( control_unit != NULL );
    assert( PC != NULL );
    assert( IR != NULL );
    assert( alu_control != NULL );
    assert( alu != NULL );

    // Inicialização dos registradores sem unidade.
    A = register_init( 0, 2 );
    ALUOut = register_init( 0, 4 );
    B = register_init( 0, 3 );
    MDR = register_init( 0, 2 );
    assert( A != NULL );
    assert( ALUOut != NULL );
    assert( B != NULL );
    assert( MDR != NULL );

    // Inicialização dos registradores com 0
    for(i = 0; i < 32; i++)
        reg[i] = 0;

    // Inicialização da RAM com 0
    for(i = 0; i < RAM_SIZE; i++){
        RAM[i] = 0;
    }

    // Escreve o programa na RAM
    write_program();

    // Salva o estado original da RAM
    for(i = 0; i < RAM_SIZE; i++){
        originalRAM[i] = RAM[i];
    }

    //Inicialização dos Shifts e Sign extend.
    st_element_data *sextend = element_init( &IR->output_15_0, &IR->sinc );
    st_element_data *sl_extended = element_init( &sextend->output, &sextend->sinc );
    st_element_data *sl_instruction = element_init( &IR->output_25_0, &IR->sinc );
    assert( sextend != NULL );
    assert( sl_extended != NULL );
    assert( sl_instruction != NULL );

    // Inicialização dos multiplexadores.
    muxers[MUX_IORD] = mux_init (
                                        &control_unit->controlSignal[IORD], &control_unit->sinc, 2,
                                        &PC->currentValue, &PC->sinc,
                                        &ALUOut->currentValue, &ALUOut->sinc
                                    );
    muxers[MUX_MEMTOREG] = mux_init (
                                                &control_unit->controlSignal[MEMTOREG], &control_unit->sinc, 2,
                                                &ALUOut->currentValue, &ALUOut->sinc,
                                                &MDR->currentValue, &MDR->sinc
                                            );
    muxers[MUX_REGDEST] = mux_init (
                                            &control_unit->controlSignal[REGDST], &control_unit->sinc, 2,
                                            &IR->output_20_16, &IR->sinc,
                                            &IR->output_15_11, &IR->sinc
                                        );
    muxers[MUX_ALUSRCA] = mux_init (
                                            &control_unit->controlSignal[ALUSRCA], &control_unit->sinc, 2,
                                            &PC->currentValue, &PC->sinc,
                                            &A->currentValue, &A->sinc
                                        );
    muxers[MUX_ALUSRCB] = mux_init (
                                            &control_unit->controlSignal[ALUSRCB], &control_unit->sinc, 4,
                                            &B->currentValue, &B->sinc,
                                            &four, NULL,
                                            &sextend->output, &sextend->sinc,
                                            &sl_extended->output, &sl_extended->sinc
                                        );
    muxers[MUX_PCSOURCE] = mux_init (
                                            &control_unit->controlSignal[PCSOURCE], &control_unit->sinc, 3,
                                            &alu->result, &alu->sinc,
                                            &ALUOut->currentValue, &ALUOut->sinc,
                                            &concatenator_output, &concatenator_sinc
                                        );

    for ( i = 0; i < MUX_NUM; ++i )
        assert( muxers[i] != NULL );

    // Inicialiazação das threads.
    pthread_create( &threads[ THREAD_UC ], NULL, controlunit_update, NULL );
    pthread_create( &threads[ THREAD_PC ], NULL, pc, NULL );
    pthread_create( &threads[ THREAD_PC_AND_OR_GATES ], NULL, pc_and_or_gates, NULL );
    pthread_create( &threads[ THREAD_MUX_IORD ], NULL, mux, (void *)muxers[MUX_IORD] );
    pthread_create( &threads[ THREAD_MEMORY ], NULL, ram_update, (void *)RAM );
    pthread_create( &threads[ THREAD_IR ], NULL, ir, NULL );
    pthread_create( &threads[ THREAD_MDR ], NULL, register_update, (void *)MDR );
    pthread_create( &threads[ THREAD_MUX_MEMTOREG ], NULL, mux, (void *)muxers[MUX_MEMTOREG] );
    pthread_create( &threads[ THREAD_MUX_REGDEST ], NULL, mux, (void *)muxers[MUX_REGDEST] );
    pthread_create( &threads[ THREAD_REGBANK ], NULL, register_bank, NULL );
    pthread_create( &threads[ THREAD_SIGNEXTEND ], NULL, signal_extend, (void *)sextend );
    pthread_create( &threads[ THREAD_SL_EXTEND ], NULL, shift_left, (void *)sl_extended );
    pthread_create( &threads[ THREAD_A ], NULL, register_update, (void *)A );
    pthread_create( &threads[ THREAD_B ], NULL, register_update, (void *)B );
    pthread_create( &threads[ THREAD_MUX_ALUSRCA ], NULL, mux, (void *)muxers[MUX_ALUSRCA] );
    pthread_create( &threads[ THREAD_MUX_ALUSRCB ], NULL, mux, (void *)muxers[MUX_ALUSRCB] );
    pthread_create( &threads[ THREAD_SL_IR ], NULL, shift_left, (void *)sl_instruction );
    pthread_create( &threads[ THREAD_CONCATENATOR ], NULL, concatenator, (void *)sl_instruction );
    pthread_create( &threads[ THREAD_ALUCONTROL ], NULL, alu_control_update, NULL );
    pthread_create( &threads[ THREAD_ALU ], NULL, alu_update, NULL );
    pthread_create( &threads[ THREAD_ALUOUT ], NULL, register_update, (void *)ALUOut );
    pthread_create( &threads[ THREAD_MUX_PCSOURCE ], NULL, mux_pcsource, (void *)muxers[MUX_PCSOURCE] );

    while ( control_unit->shutdown == 0 ) {// Aguarda o sinal para desligar.
        pthread_barrier_wait( &clock_signal ); // Sinal de clock.
        pthread_barrier_wait( &clock_signal ); // Sinal de clock.
    }

    // Espera todas as threads finalizarem
    for ( i = 0; i < THREAD_NUM; ++i )
        pthread_join( threads[i], 0 );

    // Impressão das alterações na RAM.
    print_ram();

    printf("Registradores\nPC = %X\n", PC->currentValue);
    printf("A = %d\n", A->currentValue);
    printf("B = %d\n", B->currentValue);
    printf("MDR = %d\n", MDR->currentValue);
    printf("ALUOut = %d\n", ALUOut->currentValue);
    printf("IR = %X\n", IR->currentValue);
    regbank_print();

    // Destruição dos objetos alocados.
    for ( i = 0;  i < MUX_NUM; ++i )
        mux_destroy( muxers[i] );

    pc_destroy();
    concatenator_destroy();
    controlunit_destroy();
    alu_destroy();
    alu_control_destroy();
    ir_destroy();

    element_destroy( sextend );
    element_destroy( sl_extended );
    element_destroy( sl_instruction );

    return 0;
}
