#include <stdlib.h>

#include "mux.h"
#include "global.h"
#include "controlunit.h"
#include "pc.h"

st_mux_data *mux_init( int *selector, sem_t *selector_control, int input_count, ... )
{
    st_mux_data *mux_data;

    if ( input_count < 2 // Um multiplexador deve ter ao menos 2 entradas.
        || selector == NULL // Deve ser fornecido um ponteiro a uma variável que contém o código de seleção.
        || (mux_data = (st_mux_data *) malloc( sizeof(st_mux_data) ) ) == NULL // Se houver falha ao alocar a memória.
      )
        return NULL;

    if ( sem_init( &mux_data->control, 0, 0 ) != 0 ) {
        free( mux_data );
        return NULL;
    }

    if ( (mux_data->input = (int **) malloc( sizeof(int *) * input_count ) ) == NULL ) {
        sem_destroy( &mux_data->control );
        free( mux_data );
        return NULL;
    }

    if ( (mux_data->input_control = (sem_t **) malloc( sizeof(sem_t *) * input_count ) ) == NULL ) {
        sem_destroy( &mux_data->control );
        free( mux_data->input );
        free( mux_data );
        return NULL;
    }

    int i;
    va_list vl;
    va_start( vl, input_count );
    for ( i = 0; i < input_count; ++i ) {
        if ( (mux_data->input[i] = va_arg( vl, int * ) ) == NULL ) { //As entradas devem ser válidas.
            sem_destroy( &mux_data->control );
            free( mux_data->input );
            free( mux_data );
            return NULL;
        }
        mux_data->input_control[i] = va_arg( vl, sem_t * );
    }
    va_end( vl );

    mux_data->input_count = input_count;
    mux_data->selector = selector;
    mux_data->selector_control = selector_control;

    return mux_data;
}

void mux_destroy( st_mux_data *mux_data ) {
    sem_destroy( &mux_data->control );
    free( mux_data->input );
    free( mux_data->input_control );
    free( mux_data );
}

void *mux( void *m_data ) {
    st_mux_data *mux_data = (st_mux_data *)m_data;

    while ( control_unit->shutdown == 0 ) {
        pthread_barrier_wait( &clock_signal );

        if ( mux_data->selector_control != NULL )  // Aguarda estabilização do sinal de seleção.
            sem_wait( mux_data->selector_control );

        if ( mux_data->input_control[ *mux_data->selector ] != NULL ) // Aguarda estabilização do sinal de entrada.
            sem_wait( mux_data->input_control[ *mux_data->selector ] );

        mux_data->output = *mux_data->input[ *mux_data->selector ]; // Atualiza o valor da saída.

        /* Aguarda estabilização dos demais sinais de entrada. Necessário mesmo que o sinal não seja usado,
        a fim de decrementar os demais semáforos.
        */
        int i;
        for ( i = 0; i < mux_data->input_count; ++i )
            if ( i != *mux_data->selector && mux_data->input_control[ i ] != NULL ) // Sinaliza que recebeu o sinal das demais entradas.
                sem_wait( mux_data->input_control[ i ] );

        sem_post( &mux_data->control ); // Permite acesso ao sinal de saída.

        pthread_barrier_wait( &clock_signal );
    }

    pthread_exit(0);
}

void *mux_pcsource( void *m_data ) {
    st_mux_data *mux_data = (st_mux_data *)m_data;

    while ( control_unit->shutdown == 0 ) {
        pthread_barrier_wait( &clock_signal );

        if ( mux_data->selector_control != NULL )  // Aguarda estabilização do sinal de seleção.
            sem_wait( mux_data->selector_control );

        if ( mux_data->input_control[ *mux_data->selector ] != NULL ) // Aguarda estabilização do sinal de entrada.
            sem_wait( mux_data->input_control[ *mux_data->selector ] );

        sem_wait(&PC->sinc);
        PC->nextValue = *mux_data->input[ *mux_data->selector ]; // Atualiza o valor da saída.

        /* Aguarda estabilização dos demais sinais de entrada. Necessário mesmo que o sinal não seja usado,
        a fim de decrementar os demais semáforos.
        */
        int i;
        for ( i = 0; i < mux_data->input_count; ++i )
            if ( i != *mux_data->selector && mux_data->input_control[ i ] != NULL ) // Sinaliza que recebeu o sinal das demais entradas.
                sem_wait( mux_data->input_control[ i ] );

        pthread_barrier_wait( &clock_signal );
    }

    pthread_exit(0);
}
