#include <stdarg.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <pthread.h>

#include "global.h"
#include "memory.h"
#include "mux.h"
#include "elements.h"
#include "controlunit.h"
#include "ir.h"
#include "pc.h"

st_element_data *element_init(int *input, sem_t *input_sinc) {

	if ( input_sinc == NULL )
		return NULL;

	st_element_data *element;

	if ( (element = (st_element_data*) malloc( sizeof(st_element_data) ) ) == NULL )
		return NULL;

	if ( sem_init( &element->sinc, 0, 0 ) != 0 ) {
		free( element );
		return NULL;
	}

	element->input_sinc = input_sinc;
	element->input = input;

    return element;
}

void element_destroy( st_element_data *element ) {
	sem_destroy( &element->sinc );
	free( element );
}

/* Funcionamento do Deslocador à esquerda. */
void *shift_left(void *shift_l ) {
	st_element_data *shift_left = shift_l;

	while( control_unit->shutdown == 0 ) {
		pthread_barrier_wait( &clock_signal );
		sem_wait( shift_left->input_sinc );

    	shift_left->output = (*shift_left->input << 2);
		sem_post( &shift_left->sinc );

		pthread_barrier_wait( &clock_signal );
	}
	pthread_exit(0);
}

/* Funcionamento do Extensor de Sinal. */
void *signal_extend(void *se_data ){
	st_element_data *se = se_data;

	while( control_unit->shutdown == 0 ) {
		pthread_barrier_wait( &clock_signal );
		sem_wait( se->input_sinc );

        se->output = (*se->input << 16 ) >> 16;

		sem_multipost( &se->sinc, 2 );

		pthread_barrier_wait( &clock_signal );
	}

	pthread_exit(0);
}

int concatenator_init() {
	if ( sem_init( &concatenator_sinc, 0, 0 ) != 0 )
		return 0;
	return 1;
}

void concatenator_destroy() {
	sem_destroy(&concatenator_sinc);
}

void *concatenator( void *sl ) {
	st_element_data *shift_left = sl;
	while ( control_unit->shutdown == 0 ) {
		pthread_barrier_wait( &clock_signal );
		sem_wait( &PC->sinc );
		sem_wait( &shift_left->sinc );

		concatenator_output = ( (PC->currentValue >> 28) << 28) | shift_left->output;

		sem_post( &concatenator_sinc );

		pthread_barrier_wait( &clock_signal );
	}

	pthread_exit(0);
}
