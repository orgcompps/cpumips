#include <global.h>
#include <semaphore.h>

void sem_multipost( sem_t *sem, unsigned int value ) {
    unsigned int i;
    for ( i = 0; i < value; ++i )
        sem_post( sem );
}
