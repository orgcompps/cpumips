#include <semaphore.h>

#include "pc.h"
#include "global.h"
#include "memory.h"
#include "mux.h"
#include "controlunit.h"
#include "alu.h"

st_register_data *PC = NULL;

int pc_and_or_output;
sem_t pc_and_or_output_control;

// Inicialização do PC.
int pc_init() {
    if ( PC != NULL )
        return -1;
    if ( ( PC = register_init( 0, 4 ) ) == NULL )
        return -2;
    if ( sem_init( &pc_and_or_output_control, 0, 0 ) != 0 ) {
        register_destroy( PC );
        return -3;
    }

    previousSignal = 0;
    return 1;
}

void pc_destroy() {
    register_destroy( PC );
    PC = NULL;
    sem_destroy( &pc_and_or_output_control );
}

void *pc( void *m_data ) {
    while ( control_unit->shutdown == 0 ) {
        pthread_barrier_wait( &clock_signal );

        if ( previousSignal == 1 ) // Se deve escrever novo valor de PC.
            PC->currentValue = PC->nextValue;

        sem_multipost(&PC->sinc, PC->dependencies);

        sem_wait( &pc_and_or_output_control ); // Aguarda estabilização do sinal de saída das portas AND e OR.
        previousSignal = pc_and_or_output;  // Se deve escrever novo valor de PC.

        pthread_barrier_wait( &clock_signal );
    }

    pthread_exit(0);
}

void *pc_and_or_gates( void *m_data ) {
    while ( control_unit->shutdown == 0 ) {
        pthread_barrier_wait( &clock_signal );
        sem_wait( &control_unit->sinc ); // Aguarda estabilizar os sinais de controle.
        sem_wait( &alu->sinc ); // Aguarda atualização do sinal Zero da ULA.

        pc_and_or_output = control_unit->controlSignal[PCWRITE] || ( alu->zero && control_unit->controlSignal[PCWRITECOND] );

        sem_post( &pc_and_or_output_control ); // Sinal estável na saída da porta.

        pthread_barrier_wait( &clock_signal );
    }

    pthread_exit(0);
}
