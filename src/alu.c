#include <stdlib.h>

#include "global.h"
#include "memory.h"
#include "mux.h"
#include "controlunit.h"
#include "register.h"
#include "alu.h"
#include "ir.h"

st_alu_control_data *alu_control = NULL;
st_alu_data *alu = NULL;

int alu_init(){
	if ( alu != NULL ) // Garante somente uma instância da alu durante a execução.
		return -1;
	if ( (alu = (st_alu_data *) malloc(sizeof(st_alu_data)) ) == NULL )
		return -2;
	if ( sem_init(&alu->sinc, 0, 0) != 0 ){
		free( alu );
		alu = NULL;
		return -3;
	}
	alu->control_op = &alu_control->output;

	return 1;
}

void alu_destroy(){
	sem_destroy(&alu->sinc);
	free(alu);
	alu = NULL;
}

void *alu_update( void *m_data ){

	while(control_unit->shutdown == 0){

        pthread_barrier_wait( &clock_signal );

	    sem_wait(&alu_control->sinc);
		sem_wait(&muxers[MUX_ALUSRCA]->control);	// Aguarda a estabilização da saída do multiplexador ALUSRCA.
		sem_wait(&muxers[MUX_ALUSRCB]->control);	// Aguarda a estabilização da saída do multiplexador ALUSRCB.

		switch( *alu->control_op ){
			//Operações ADD e SUB.
			case alu_add :
				alu->result = (muxers[MUX_ALUSRCA]->output + muxers[MUX_ALUSRCB]->output); 	break;
			case alu_sub :
				alu->result = (muxers[MUX_ALUSRCA]->output - muxers[MUX_ALUSRCB]->output); 	break;

			//Operações AND e OR bit-a-bit.
			case alu_and :
				alu->result = (muxers[MUX_ALUSRCA]->output & muxers[MUX_ALUSRCB]->output);	break;
			case alu_or :
				alu->result = (muxers[MUX_ALUSRCA]->output | muxers[MUX_ALUSRCB]->output);	break;

			//Operação SLT.
			case alu_slt:
				alu->result = ( (muxers[MUX_ALUSRCA]->output < muxers[MUX_ALUSRCB]->output) ? 1 : 0 );	break;

			default:
				alu->result = 0;	break;
		}

		sem_wait(&ALUOut->sinc);
        ALUOut->nextValue = alu->result;
		alu->zero = ( (alu->result == 0) ? 1 : 0);

	    sem_multipost(&alu->sinc, 2);

		pthread_barrier_wait( &clock_signal );
	}

	pthread_exit(0);
}

int alu_control_init(){
	if ( alu_control != NULL )
		return -1;
	if ( (alu_control = (st_alu_control_data *) malloc(sizeof(st_alu_control_data)) ) == NULL )
		return -2;
	if ( sem_init( &alu_control->sinc, 0, 0 ) != 0) {
		free(alu_control);
		return -3;
	}

	alu_control->input = &IR->output_5_0;
	alu_control->alu_op_input = &control_unit->controlSignal[ALUOP];

	return 1;
}

void alu_control_destroy(){
	sem_destroy( &alu_control->sinc );
	free(alu_control);
	alu_control = NULL;
}

void *alu_control_update( void *m_data ){

	while(control_unit->shutdown == 0){

        pthread_barrier_wait( &clock_signal );
		sem_wait( &control_unit->sinc );
		sem_wait( &IR->sinc );	    // Aguarda a estabilização da saída do IR.

		//Instruções tipo R
		if( *alu_control->alu_op_input == 0b10 || *alu_control->alu_op_input == 0b11 ){

			if( *alu_control->input == funct_add )	alu_control->output = alu_add;
			if( *alu_control->input == funct_sub ) 	alu_control->output = alu_sub;
			if( *alu_control->input == funct_and ) 	alu_control->output = alu_and;
			if( *alu_control->input == funct_or )  	alu_control->output = alu_or;
			if( *alu_control->input == funct_slt)  	alu_control->output = alu_slt;

		}else{
			//Instruções LW ou SW.
			if( *alu_control->alu_op_input == 0b00 )	alu_control->output = alu_add;
			//Instrução BEQ.
			if( *alu_control->alu_op_input == 0b01 )	alu_control->output = alu_sub;
		}
		sem_post( &alu_control->sinc );

		pthread_barrier_wait( &clock_signal );
	}

	pthread_exit(0);
}
