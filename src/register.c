#include <stdlib.h>

#include "global.h"
#include "memory.h"
#include "mux.h"
#include "controlunit.h"
#include "register.h"
#include "ir.h"

st_register_data *register_init( int value, int dependent_components) {
    st_register_data *register_data;

    if ( dependent_components <= 0 || (register_data = (st_register_data *) malloc( sizeof(st_register_data) ) ) == NULL )
        return NULL;

    if ( sem_init( &register_data->sinc , 0 ,0) != 0 ){
        free( register_data );
        return NULL;
    }

    register_data->dependencies = dependent_components;
    register_data->nextValue = 0;
    register_data->currentValue = value;

    return register_data;
}

void register_destroy( st_register_data *register_data ) {
    sem_destroy( &register_data->sinc );
    free( register_data );
}

void *register_update( void *m_data ) {
    st_register_data *register_data = (st_register_data *)m_data;

    while ( control_unit->shutdown == 0 ) {
        pthread_barrier_wait( &clock_signal );

        register_data->currentValue = register_data->nextValue;

        sem_multipost( &register_data->sinc, register_data->dependencies );

        pthread_barrier_wait( &clock_signal );
    }

    pthread_exit(0);
}

void *register_bank( void *m_data ) {
    while(control_unit->shutdown == 0) {
        pthread_barrier_wait( &clock_signal ); // Aguarda sinal de clock.

        sem_wait( &control_unit->sinc );
        sem_wait( &IR->sinc );
        sem_wait( &A->sinc );
        A->nextValue = reg[ IR->output_25_21 ];
        sem_wait( &B->sinc );
        B->nextValue = reg[ IR->output_20_16 ];

        sem_wait( &muxers[MUX_REGDEST]->control ); // Aguarda a estabilização da saída do multiplexador REGDST.
        sem_wait( &muxers[MUX_MEMTOREG]->control ); // Aguarda a estabilização da saída multiplexador MEMTOREG.
        if ( control_unit->controlSignal[REGWRITE] )
            reg[ muxers[MUX_REGDEST]->output ] = muxers[MUX_MEMTOREG]->output;

        pthread_barrier_wait( &clock_signal );
    }

    pthread_exit(0);
}

void regbank_print() {
    int i;
    printf("$zero = %d\n", reg[0]);
    printf("$at = %d\n", reg[1]);
    for ( i = 2; i < 4; ++i )
        printf("$v%d = %d\n", i - 2, reg[i]);
    for ( i = 4; i < 8; ++i )
        printf("$a%d = %d\n", i - 4, reg[i]);
    for ( i = 8; i < 16; ++i )
        printf("$t%d = %d\n", i - 8, reg[i]);
    for ( i = 16; i < 24; ++i )
        printf("$s%d = %d\n", i - 16, reg[i]);
    for ( i = 24; i < 26; ++i )
        printf("$t%d = %d\n", i - 16, reg[i]);
    for ( i = 26; i < 28; ++i )
        printf("$k%d = %d\n", i - 26, reg[i]);
    printf("$gp = %d\n", reg[28]);
    printf("$sp = %d\n", reg[29]);
    printf("$fp = %d\n", reg[30]);
    printf("$ra = %d\n", reg[31]);
}
