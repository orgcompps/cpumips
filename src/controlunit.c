#include <stdlib.h>

#include "controlunit.h"
#include "ir.h"

st_control_unit *control_unit = NULL;

int controlunit_init( int state )
{
    if ( control_unit != NULL )
        return 0;

    if ( (control_unit = (st_control_unit *) malloc( sizeof(st_control_unit) ) ) == NULL )
        return -1;

    if ( sem_init( &control_unit->sinc, 0, 0 ) != 0 ) {
        free( control_unit );
        return -2;
    }

    control_unit->currentState = state;
    control_unit->shutdown = 0;

    return 1;
}

void *controlunit_update()
{
    int i;
    while ( control_unit->shutdown == 0 ) {
        pthread_barrier_wait( &clock_signal );

        // Seta os sinais para 0 como padrão
        for(i = 0; i < SIGNAL_NUM; i++)
        {
            control_unit->controlSignal[i] = 0;
        }

        sem_wait(&IR->sinc); // Espera por IR para poder usar o opcode

        // Funcionamento da maquina de estados
        switch(control_unit->currentState)
        {
            case INSTRUCTION_FETCH:
                control_unit->controlSignal[MEMREAD] = 1;
                control_unit->controlSignal[ALUSRCA] = 0;
                control_unit->controlSignal[IORD] = 0;
                control_unit->controlSignal[IRWRITE] = 1;
                control_unit->controlSignal[ALUSRCB] = 1;
                control_unit->controlSignal[ALUOP] = 0;
                control_unit->controlSignal[PCWRITE] = 1;
                control_unit->controlSignal[PCSOURCE] = 0;

                control_unit->currentState = INSTRUCTION_DECODE;
                break;

            case INSTRUCTION_DECODE:
                control_unit->controlSignal[ALUSRCA] = 0;
                control_unit->controlSignal[ALUSRCB] = 3;
                control_unit->controlSignal[ALUOP] = 0;

                switch(IR->output_31_26)
                {
                    case INSTRUCTION_J:
                        control_unit->currentState = JUMP_COMPLETION;
                        break;

                    case INSTRUCTION_BEQ:
                        control_unit->currentState = BRANCH_COMPLETION;
                        break;

                    case INSTRUCTION_R:
                        control_unit->currentState = EXECUTION;
                        break;

                    case INSTRUCTION_LW:
                    case INSTRUCTION_SW:
                        control_unit->currentState = MEMORY_ADDRESS_COMPUTATION;
                        break;

                    default:
                        control_unit->shutdown = 1;
                        break;
                }
                break;

            case JUMP_COMPLETION:
                control_unit->controlSignal[PCWRITE] = 1;
                control_unit->controlSignal[PCSOURCE] = 2;

                control_unit->currentState = INSTRUCTION_FETCH;
                break;

            case BRANCH_COMPLETION:
                control_unit->controlSignal[ALUSRCA] = 1;
                control_unit->controlSignal[ALUSRCB] = 0;
                control_unit->controlSignal[ALUOP] = 1;
                control_unit->controlSignal[PCWRITECOND] = 1;
                control_unit->controlSignal[PCSOURCE] = 1;

                control_unit->currentState = INSTRUCTION_FETCH;
                break;

            case EXECUTION:
                control_unit->controlSignal[ALUSRCA] = 1;
                control_unit->controlSignal[ALUSRCB] = 0;
                control_unit->controlSignal[ALUOP] = 2;

                control_unit->currentState = RTYPE_COMPLETION;
                break;

            case RTYPE_COMPLETION:
                control_unit->controlSignal[REGDST] = 1;
                control_unit->controlSignal[REGWRITE] = 1;
                control_unit->controlSignal[MEMTOREG] = 0;

                control_unit->currentState = INSTRUCTION_FETCH;
                break;

            case MEMORY_ADDRESS_COMPUTATION:
                control_unit->controlSignal[ALUSRCA] = 1;
                control_unit->controlSignal[ALUSRCB] = 2;
                control_unit->controlSignal[ALUOP] = 0;

                if(IR->output_31_26 == INSTRUCTION_SW)
                    control_unit->currentState = MEMORY_ACCESS_SW;
                else
                    control_unit->currentState = MEMORY_ACCESS_LW;
                break;

            case MEMORY_ACCESS_SW:
                control_unit->controlSignal[MEMWRITE] = 1;
                control_unit->controlSignal[IORD] = 1;

                control_unit->currentState = INSTRUCTION_FETCH;
                break;

            case MEMORY_ACCESS_LW:
                control_unit->controlSignal[MEMREAD] = 1;
                control_unit->controlSignal[IORD] = 1;

                control_unit->currentState = MEMORY_READ_COMPLETION;
                break;

            case MEMORY_READ_COMPLETION:
                control_unit->controlSignal[REGDST] = 0;
                control_unit->controlSignal[REGWRITE] = 1;
                control_unit->controlSignal[MEMTOREG] = 1;

                control_unit->currentState = INSTRUCTION_FETCH;
                break;

            default:
                break;
        }

        sem_multipost( &control_unit->sinc, 11 );

        pthread_barrier_wait( &clock_signal );
    }

    pthread_exit(0);
}

void controlunit_destroy()
{
    sem_destroy( &control_unit->sinc );
    free(control_unit);
}
