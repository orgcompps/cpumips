#include "memory.h"
#include "global.h"
#include "controlunit.h"
#include "register.h"
#include "mux.h"
#include "ir.h"

void *ram_update(void *m_data)
{
    int *ram = (int *)m_data;
    int address, write_data;

    while(control_unit->shutdown == 0)
    {
        pthread_barrier_wait( &clock_signal );

        sem_wait( &control_unit->sinc );

        sem_wait( &muxers[MUX_IORD]->control );
        address = muxers[MUX_IORD]->output >> 2;

        sem_wait( &B->sinc );
        write_data = B->currentValue;

        if(control_unit->controlSignal[MEMWRITE] == 1)
        {
            ram[address] = write_data;
        }

        sem_wait( &IR->sinc );
        sem_wait( &MDR->sinc );
        if(control_unit->controlSignal[MEMREAD] == 1)
        {
            IR->nextValue = ram[address];
            MDR->nextValue = ram[address];
        }

        pthread_barrier_wait( &clock_signal );
    }

    pthread_exit(0);
}

void print_ram(){
    int i;
    printf("Posições de memória alteradas\n");
    for(i = 0; i < RAM_SIZE; i++){
        if ( originalRAM[i] != RAM[i] ){
            printf("RAM[%d]: %d\n", i, RAM[i]);
        }
    }
}
